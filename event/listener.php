<?php

/**
 *
 * @package       phpBB Extension - DC-Trad BBCode
 * @copyright (c) 2022 Ady - https://gitlab.com/adynemo
 * @license       http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\dctbbcode\event;

use phpbb\auth\auth;
use phpbb\event\data;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	/**
	 * @var auth
	 */
	private $auth;

	public function __construct(auth $auth)
	{
		$this->auth = $auth;
	}

	static public function getSubscribedEvents()
	{
		return [
			'core.permissions'                  => 'permissions',
			'core.posting_modify_template_vars' => 'give_variables_to_posting_template',
		];
	}

	public function permissions(data $event)
	{
		$permissions['m_dctbbcode'] = [
			'lang' => 'ACL_M_DCTBBCODE',
			'cat'  => 'ady_dct',
		];
		$event['permissions'] = array_merge($event['permissions'], $permissions);
	}

	public function give_variables_to_posting_template(data $event): void
	{
		$data = [
			'ADY_DCTBBCODE_ALLOWED' => $this->auth->acl_get('m_dctbbcode'),
		];

		$event['page_data'] = array_merge($event['page_data'], $data);
	}
}
