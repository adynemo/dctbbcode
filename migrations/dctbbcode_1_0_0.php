<?php

/**
 * @package   phpBB Extension - DC-Trad BBCode
 * @copyright 2022 Ady
 * @license   http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 */

namespace ady\dctbbcode\migrations;

class dctbbcode_1_0_0 extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return [
			['permission.add', ['m_dctbbcode']],
		];
	}
}
