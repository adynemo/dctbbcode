(function() {
	const modal = document.getElementById("adydctbbcode-modal");
	const span = document.getElementsByClassName("close")[0];
	span.onclick = function() {
		modal.style.display = "none";
	}

	window.onclick = function(event) {
		if (event.target === modal) {
			modal.style.display = "none";
		}
	}
	const button = document.getElementById('adydctbbcode-button');
	button.addEventListener('click', function() {
		modal.style.display = "block";
		const choices = modal.querySelectorAll('img.adydctbbcode-choices');
		for (const choice of choices) {
			choice.addEventListener('click', handleEventOnChoicesClick);
		}
	})

	function handleEventOnChoicesClick(e) {
		const image = e.target.src;
		modal.style.display = "none";
		const url = prompt('Entrez une URL (OwnCloud, par exemple) ou laissez vide.');
		process(url, image);
	}

	function process(url, image) {
		const imgCode = "[img]" + image + "[/img]";
		const textCode = '' !== url && null !== url ? "[url=" + url + "]" + imgCode + "[/url]" : imgCode;
		insertAtCaret('message', textCode);
	}

	function insertAtCaret(areaId, text) {
		const FF = 1;
		const IE = 2;
		const textarea = document.getElementById(areaId);
		if (!textarea) {
			return;
		}

		const scrollPos = textarea.scrollTop;
		let strPos = 0;
		const br = ((textarea.selectionStart || textarea.selectionStart === 0) ?
			FF : (document.selection ? IE : false));
		if (br === IE) {
			textarea.focus();
			const range = document.selection.createRange();
			range.moveStart('character', -textarea.value.length);
			strPos = range.text.length;
		} else if (br === FF) {
			strPos = textarea.selectionStart;
		}

		const front = (textarea.value).substring(0, strPos);
		const back = (textarea.value).substring(strPos, textarea.value.length);
		textarea.value = front + text + back;
		strPos = strPos + text.length;
		if (br === IE) {
			textarea.focus();
			const ieRange = document.selection.createRange();
			ieRange.moveStart('character', -textarea.value.length);
			ieRange.moveStart('character', strPos);
			ieRange.moveEnd('character', 0);
			ieRange.select();
		} else if (br === FF) {
			textarea.selectionStart = strPos;
			textarea.selectionEnd = strPos;
			textarea.focus();
		}

		textarea.scrollTop = scrollPos;
	}
})()
